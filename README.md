# Car Manager #

Transfer car property between private citizens.

### Quickstart ###

* [Summary of set up](./docs/setup.md)
* [Configuration](./docs/configuration.md)
* [How to run tests](docs/run_tests.md)
* [Deployment instructions](./docs/deployment.md)

### Practices ###

The project follow a typical Git Flow development branching model.

- Production releases branch: [master] 
- Next release branch: [develop]
- Feature branches [feature/] 
- Bugfix branches [bugfix/] 
- Release branches? [release/] 
- Hotfix branches? [hotfix/] 
- Support branches? [support/]  

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
