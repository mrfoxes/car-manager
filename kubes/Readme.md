# Kubernetes deployment spec

In order to manage multiple environments the specification is build with **[Kustomize](https://kubectl.docs.kubernetes.io/pages/reference/kustomize.html#configurations)**

Refer to the Makefile in order to build the output.
