# Intro #

## Repository structure ##

    ├── docs
    ├── infra
    ├── kubes
    └── services

- **docs**: generic documentation folder
- **[infra](../infra/_readme.md)**: infrastructure provisioning
- **[kubes](../kubes/Readme.md)**: kubernetes deployment manifests
- **[services](../services/Readme.md)**: contains all the services
