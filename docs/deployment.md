# Deployment

The app will be released throwout containers.

Deployment configurations are available for:

- Docker Compose
- Kubernetes

## Infrastructure provisioning

Refer to the infrastructure section in order to provision the requirement and deploy the application. 
