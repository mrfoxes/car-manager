# Infrastructure #

This folder contains all the necessaries files to provision the infrastructure to run the application.

## Setup IBM provider

### Installation
1. Install Terraform and terraform-ibm provider
First wget the terraform zip file, unzip, and cp to /bin from https://www.terraform.io/downloads.html.

Then wget zip, unzip, cp to /bin from https://github.com/IBM-Cloud/terraform-provider-ibm/releases.

Create a symbolic link between the Terraform and terraform-ibm provider files by using: ln -s terraform-provider-ibm_vTHE_RELEASE_YOU_DOWNLOADED terraform-provider-ibm

### 2. Set up .terraformrc

Set up your CLI configuration file ~/.terraformrc by adding the following code to it – create the file if it doesn’t exists:

        providers {
            ibm = "<LOCATION_OF_INSTALLATION>/bin/terraform-provider-ibm"
        }

## Development

Initialize terraform modules

    terraform init
