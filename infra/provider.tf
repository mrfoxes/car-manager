variable "ibmcloud_api_key" {
  type = string
}
variable "ibm_cloud_region" {
  type = string
}
variable "aws_region" {
  type = string
}

provider "ibm" {
  ibmcloud_api_key = var.ibmcloud_api_key
  generation = 1
  region = var.ibm_cloud_region
}

provider "aws" {
  region = var.aws_region
  access_key = "my-access-key"
  secret_key = "my-secret-key"
}
