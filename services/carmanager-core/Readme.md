# Car Manager - Core

Car Property management application based on Django

## Development

Start all services

    docker-compose up -d
    
Web server listening at http://localhost:8000
    
Apply the initial migrations

    docker-compose exec core python manage.py migrate
    
Load sample car database

    docker-compose exec core python manage.py loaddata --app core ./carmanager/cars_database/fixtures/cars_database.json
    
Generate fake profiles

    python manage.py generate_fake_profiles {{COUNT}}
    
Assign some cars
    
    python manage.py buy_some_cars 

## Commands

Generate fake profiles to use for testing purposes.

Password for each profile: password

    python manage.py generate_fake_profiles {{COUNT}}
    
Reset data for car property app
    
    python manage.py reset_property_data
    
Buy some cars for all users

    python manage.py buy_some_cars
