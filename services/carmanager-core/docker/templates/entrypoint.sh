#!/bin/sh

run_web_server()
{
    nginx

    python manage.py collectstatic --noinput

    python manage.py runserver 0.0.0.0:8000
}

run_beat()
{
    celery -A carmanager beat -l info
}

run_worker()
{
    celery -A carmanager -Q "${WORKERS_QUEUE:=priority_high,default}" -B -l info
}

if [ "$#" -lt 1 ]; then
    run_web_server

    else
        if [ "$1" = "--entrypoint" ]; then
            if [ "$3" = "beat" ] ; then
                    run_beat
                elif [ "$3" = "worker" ] ; then
                    run_worker
                else
                    run_web_server
                fi
        else
            if [ "$1" = "beat" ] ; then
                run_beat
            elif [ "$1" = "worker" ] ; then
                run_worker
            else
                run_web_server
            fi
        fi
fi
