from uuid import uuid4

from django.db import models


class Car(models.Model):
    id = models.UUIDField(primary_key=True, unique=True, null=False, blank=False, default=uuid4)
    make = models.CharField(max_length=30, blank=False, null=False)
    model = models.CharField(max_length=30, blank=False, null=False)
    platform_generation_number = models.CharField(max_length=30, default='', null=True, blank=True)
    european_world_classification = models.CharField(max_length=30, default='', null=True, blank=True)
    production_years = models.CharField(max_length=30, default='', null=True, blank=True)
    american_classification = models.CharField(max_length=30, default='', null=True, blank=True)
    model_years_us_cana = models.CharField(max_length=30, default='', null=True, blank=True)
    country_of_orig = models.CharField(max_length=30, default='', null=True, blank=True)

    def __str__(self):
        return f'{self.make} / {self.model}'

    class Meta:
        pass
