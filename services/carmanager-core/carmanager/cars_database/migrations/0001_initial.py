# Generated by Django 3.0.4 on 2020-03-29 16:43

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False, unique=True)),
                ('make', models.CharField(max_length=30)),
                ('model', models.CharField(max_length=30)),
                ('platform_generation_number', models.CharField(blank=True, default='', max_length=30, null=True)),
                ('european_world_classification', models.CharField(blank=True, default='', max_length=30, null=True)),
                ('production_years', models.CharField(blank=True, default='', max_length=30, null=True)),
                ('american_classification', models.CharField(blank=True, default='', max_length=30, null=True)),
                ('model_years_us_cana', models.CharField(blank=True, default='', max_length=30, null=True)),
                ('country_of_orig', models.CharField(blank=True, default='', max_length=30, null=True)),
            ],
        ),
    ]
