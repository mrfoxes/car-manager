from django.contrib import admin

from carmanager.cars_database.models import Car


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    pass
