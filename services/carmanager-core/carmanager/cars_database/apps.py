from django.apps import AppConfig


class CarsDatabaseConfig(AppConfig):
    name = 'cars_database'
