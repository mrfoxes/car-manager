from .models import CarTransferRequestView, CarProperty


def approve_car_transfer(transfer_id):
    transfer_request: CarTransferRequestView = CarTransferRequestView.objects.get(id=transfer_id)

    CarProperty.objects.create(
        license_plate=transfer_request.license_plate,
        car=transfer_request.car,
        status=CarProperty.PropertyStatus.TRANSFERRED,
        owner=transfer_request.new_owner,
        new_owner=None
    )
