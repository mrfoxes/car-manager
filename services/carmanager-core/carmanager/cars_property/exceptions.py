class CannotTransferException(Exception):
    pass


class CannotStartTransferException(Exception):
    pass
