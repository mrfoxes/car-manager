import random

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from faker import Faker

from carmanager.cars_database.models import Car
from carmanager.cars_property.models import CarProperty

User = get_user_model()


class Command(BaseCommand):
    help = "Buy some cars for all users"
    fake: Faker = Faker(['it_IT'])

    def add_arguments(self, parser):
        # parser.add_argument('count', type=int)
        pass

    def get_license_plate(self):
        return self.fake.license_plate()

    def handle(self, *args, **options):
        users = User.objects.all().filter(is_staff=False, is_superuser=False).only('id')
        cars_count = Car.objects.all().count()

        for u in users:
            chosen_car = Car.objects.all()[random.randint(0, cars_count - 1)]

            CarProperty.objects.create(
                license_plate=self.get_license_plate(),
                owner=u,
                status=CarProperty.PropertyStatus.TRANSFERRED,
                car=chosen_car
            )
