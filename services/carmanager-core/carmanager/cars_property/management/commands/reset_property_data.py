from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from faker import Faker

from carmanager.cars_property.models import CarProperty, CarPropertyView, CarTransferRequestView

User = get_user_model()


class Command(BaseCommand):
    help = "Reset car property data"
    fake: Faker = Faker(['it_IT'])

    def add_arguments(self, parser):
        pass

    def get_license_plate(self):
        return self.fake.license_plate()

    def handle(self, *args, **options):
        CarProperty.objects.all().delete()
        CarPropertyView.objects.all().delete()
        CarTransferRequestView.objects.all().delete()
