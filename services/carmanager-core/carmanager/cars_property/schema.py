import graphene
import carmanager.cars_property.resolvers.property_transfer.get_my_cars
import carmanager.cars_property.resolvers.property_transfer.transfer_car_property


class Mutation():
    transfer_car_property = carmanager.cars_property.resolvers.property_transfer.transfer_car_property.TransferCarProperty.Field()


class Query(
    carmanager.cars_property.resolvers.property_transfer.get_my_cars.Query,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query)
