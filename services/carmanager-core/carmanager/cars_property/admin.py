from django.contrib import admin

from carmanager.cars_property.models import CarProperty, CarPropertyView, CarTransferRequestView
from carmanager.cars_property.services import approve_car_transfer


@admin.register(CarProperty)
class CarProperty(admin.ModelAdmin):
    list_display = (
        'id',
        'status',
        'license_plate',
        'car',
        'owner',
        'new_owner',
        'created',
        'modified',
    )


@admin.register(CarPropertyView)
class CarPropertyView(admin.ModelAdmin):
    list_display = (
        'id',
        'license_plate',
        'status',
        'car',
        'owner',
        'created',
        'modified',
    )


def approve_transfer(modeladmin, request, queryset: [CarTransferRequestView]):
    for q in queryset:
        approve_car_transfer(q.id)


@admin.register(CarTransferRequestView)
class CarPropertyView(admin.ModelAdmin):
    list_display = (
        'id',
        'license_plate',
        'status',
        'car',
        'owner',
        'new_owner',
        'created',
        'modified',
    )

    actions = [
        approve_transfer
    ]
