import logging

import graphene
from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from graphene_django.forms.mutation import DjangoFormMutation
from graphql_jwt.decorators import login_required

from carmanager.cars_property.models import CarProperty

logger = logging.getLogger(__name__)

User = get_user_model()


def validate_accept(value):
    if not value:
        raise ValidationError(
            _('Must accept in order to start transfer'),
            params={'value': value},
        )


def license_plate_validator(value):
    logger.debug('Validate license plate can be transfered')
    actual_car_state: CarProperty = CarProperty.objects.order_by('-created').filter(
        license_plate__exact=value
    ).first()

    if actual_car_state is None:
        raise ValidationError(
            _('No vehicle found in the system for specified license plate number.'),
            params={'value': value},
        )
    else:
        actual_car_state.can_start_transfer()

    return


class TransferCarPropertyForm(forms.Form):
    new_owner = forms.EmailField(required=True)
    license_plate = forms.CharField(required=True, validators=[license_plate_validator])
    accept = forms.BooleanField(validators=[validate_accept], required=False)

    class Meta:
        fields = (
            'new_owner',
            'license_plate',
            'accept',
        )


class TransferCarProperty(DjangoFormMutation):
    class Meta:
        form_class = TransferCarPropertyForm

    success = graphene.Field(graphene.Boolean, default_value=False)

    @classmethod
    @login_required
    def perform_mutate(cls, form: TransferCarPropertyForm, info):
        user = info.context.user
        new_owner, created = User.objects.get_or_create(email=form.cleaned_data['new_owner'])

        if created:
            # Execute some logic if the users is not on the system yet
            pass

        property_status: CarProperty = CarProperty.objects.filter(
            license_plate__exact=form.cleaned_data['license_plate'],
            owner=user
        ).order_by('-created').first()

        if property_status is None:
            raise Exception(
                _('No car available to transfer. Please be sure to own the vehicle before starting a transfer.')
            )

        if new_owner.email == user.email:
            raise Exception(
                _('Cannot transfer vehicle to the same owner')
            )

        CarProperty.objects.create(
            license_plate=property_status.license_plate,
            owner=user,
            status=CarProperty.PropertyStatus.PENDING_TRANSFER,
            new_owner=new_owner,
            car=property_status.car
        )

        return cls(
            success=True,
        )
