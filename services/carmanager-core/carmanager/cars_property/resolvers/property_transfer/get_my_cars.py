import graphene
from graphql_jwt.decorators import login_required

from carmanager.cars_property.models import CarPropertyView
from carmanager.cars_property.resolvers.property_transfer.type_defs import CarPropertyViewType


class Query(object):
    get_my_cars = graphene.List(CarPropertyViewType)

    @login_required
    def resolve_get_my_cars(self, info, **kwargs):
        return CarPropertyView.objects.all().filter(owner=info.context.user)
