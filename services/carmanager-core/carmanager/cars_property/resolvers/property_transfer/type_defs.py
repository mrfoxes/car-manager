from graphene_django import DjangoObjectType

from carmanager.cars_database.models import Car
from carmanager.cars_property.models import CarPropertyView


class CarType(DjangoObjectType):
    class Meta:
        model = Car
        name = 'Car'


class CarPropertyViewType(DjangoObjectType):
    class Meta:
        model = CarPropertyView
        name = 'CarPropertyView'
