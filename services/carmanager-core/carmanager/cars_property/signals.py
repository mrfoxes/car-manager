from django.db.models.signals import post_save
from django.dispatch import receiver

from carmanager.cars_property.models import CarProperty, CarPropertyView, CarTransferRequestView


# Post save handler for Car Property
# Ideally used to trigger some events
@receiver(post_save, sender=CarProperty)
def car_property_post_save(sender, instance: CarProperty, **kwargs):
    # Sent messages only when an object is created because audit log means immutable series of events
    if instance.created:
        obj, created = CarPropertyView.objects.get_or_create(
            license_plate=instance.license_plate,
            car=instance.car,
        )

        CarPropertyView.objects.filter(id=obj.id).update(
            owner=instance.owner,
            status=instance.status
        )

        if instance.status == CarProperty.PropertyStatus.PENDING_TRANSFER:
            CarTransferRequestView.objects.create(
                license_plate=instance.license_plate,
                car=instance.car,
                owner=instance.owner,
                new_owner=instance.new_owner,
                status=instance.status
            )
        elif instance.status == CarProperty.PropertyStatus.TRANSFERRED:
            CarTransferRequestView.objects.filter(license_plate=instance.license_plate).delete()
