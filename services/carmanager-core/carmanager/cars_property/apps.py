from django.apps import AppConfig


class CarsPropertyConfig(AppConfig):
    name = 'carmanager.cars_property'

    def ready(self):
        import carmanager.cars_property.signals
