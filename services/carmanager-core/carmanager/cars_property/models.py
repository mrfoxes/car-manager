from uuid import uuid4

from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import TextChoices
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel

from carmanager.cars_property.exceptions import CannotTransferException, CannotStartTransferException

User = get_user_model()


class CarProperty(TimeStampedModel):
    class PropertyStatus(TextChoices):
        PENDING_TRANSFER = 'PENDING_TRANSFER', _('Pending transfer')
        TRANSFERRED = 'TRANSFERRED', _('Transferred')

    id = models.UUIDField(primary_key=True, null=False, blank=False, unique=True, default=uuid4, db_index=True)
    license_plate = models.CharField(max_length=20, blank=False, null=False, default=None, db_index=True)
    car = models.ForeignKey(
        'cars_database.Car',
        default=None,
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING
    )
    status = models.CharField(max_length=20, blank=False, null=False, default=None, choices=PropertyStatus.choices,
                              db_index=True)
    owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='car_property_owner',
        related_query_name='car_property_owner',
        db_index=True
    )
    new_owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='car_property_new_owner',
        related_query_name='car_property_new_owner',
        db_index=True
    )

    def __str__(self):
        return f'{self.car} / {self.owner} / {self.status}'

    def can_start_transfer(self):
        if self.status == self.PropertyStatus.TRANSFERRED:
            return True

        raise CannotStartTransferException(_('Car property transfer already in progress'))

    def can_transfer(self):
        if self.status == self.PropertyStatus.PENDING_TRANSFER:
            return True

        raise CannotTransferException(_('Cannot complete transfer'))


class CarPropertyView(TimeStampedModel):
    """
    Handle the query for current car property status

    The view reflect the current status for a vehicle

    Listen to just ALL events
    """
    id = models.UUIDField(primary_key=True, null=False, blank=False, unique=True, default=uuid4, db_index=True)
    license_plate = models.CharField(max_length=20, blank=False, null=False, default=None, db_index=True)
    car = models.ForeignKey(
        'cars_database.Car',
        default=None,
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING, db_index=True,
        related_name='+'
    )
    owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='+',
        db_index=True
    )
    status = models.CharField(
        max_length=20,
        blank=True,
        null=True,
        default=None,
        choices=CarProperty.PropertyStatus.choices,
        db_index=True
    )

    class Meta:
        unique_together = ('owner', 'license_plate', 'car',)


class CarTransferRequestView(TimeStampedModel):
    """
    Handle the queue for pending requests that need to be validate

    Listen to just all events

    Add if PENDING_TRANSFER
    Remove if TRANSFERRED

    """
    id = models.UUIDField(primary_key=True, null=False, blank=False, unique=True, default=uuid4, db_index=True)
    license_plate = models.CharField(max_length=20, blank=False, null=False, default=None, db_index=True)
    car = models.ForeignKey(
        'cars_database.Car',
        default=None,
        null=False,
        blank=False,
        on_delete=models.DO_NOTHING, db_index=True,
        related_name='+'
    )
    owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='+',
        db_index=True
    )
    status = models.CharField(
        max_length=20,
        blank=True,
        null=True,
        default=None,
        choices=CarProperty.PropertyStatus.choices,
        db_index=True
    )
    new_owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        default=None,
        on_delete=models.DO_NOTHING,
        related_name='+',
        db_index=True
    )

    class Meta:
        unique_together = ('owner', 'license_plate', 'car',)
