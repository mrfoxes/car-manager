import graphene
import graphql_jwt
from django.utils.translation import ugettext_lazy as _
import carmanager.core.resolvers.query.health
import carmanager.cars_property.resolvers.property_transfer.get_my_cars
import carmanager.cars_property.schema
import carmanager.users.schema


class AuthMutation(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field(
        description=_('Update an existing folder')
    )
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Mutation(
    AuthMutation,
    carmanager.cars_property.schema.Mutation,
):
    pass


class Query(
    carmanager.core.resolvers.query.health.Query,
    carmanager.cars_property.resolvers.property_transfer.get_my_cars.Query,
    carmanager.users.schema.Query,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
