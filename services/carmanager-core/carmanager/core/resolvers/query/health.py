import graphene
from graphene.types.generic import GenericScalar


class Query(object):
    health = graphene.Field(graphene.Boolean)

    def resolve_health(self, info, **kwargs):
        return True
