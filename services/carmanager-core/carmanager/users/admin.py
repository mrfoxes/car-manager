from django.contrib import admin

from carmanager.users.models import Profile, Preferences


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'ssn',
    )


@admin.register(Preferences)
class PreferencesAdmin(admin.ModelAdmin):
    fields = (
        'user',
    )
