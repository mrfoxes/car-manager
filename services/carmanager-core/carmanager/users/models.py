from uuid import uuid4

from django.contrib.auth import get_user_model
from django.db import models
from django_extensions.db.models import TimeStampedModel

User = get_user_model()


class Profile(TimeStampedModel):
    id = models.UUIDField(primary_key=True, unique=True, null=False, blank=False, default=uuid4)
    user = models.OneToOneField(
        User,
        null=False,
        blank=False,
        default=None,
        on_delete=models.CASCADE,
        related_name='user_profile',
        related_query_name='user_profile',
        db_index=True
    )
    job = models.CharField(max_length=100, default=None, blank=True, null=True)
    company = models.CharField(max_length=100, default=None, blank=True, null=True)
    ssn = models.CharField(max_length=30, default=None, blank=True, null=True, db_index=True)
    residence = models.CharField(max_length=150, default=None, blank=True, null=True)
    blood_group = models.CharField(max_length=5, default=None, blank=True, null=True)
    sex = models.CharField(max_length=3, null=True, blank=True, default=None)
    address = models.CharField(max_length=150, blank=True, null=True, default='')
    birthdate = models.DateField(null=True, blank=True, default=None)

    def __str__(self):
        return f'Profile: {self.user.email}'

    class Meta:
        pass


class Preferences(TimeStampedModel):
    id = models.UUIDField(primary_key=True, unique=True, null=False, blank=False, default=uuid4)
    user = models.OneToOneField(
        User,
        null=False,
        blank=False,
        default=None,
        on_delete=models.CASCADE,
        related_name='user_preferences',
        related_query_name='user_preferences',
        db_index=True
    )

    def __str__(self):
        return f'Preferences: {self.user.email}'

    class Meta:
        pass
