import graphene
import carmanager.users.resolvers.queries.get_my_profile


class Mutation():
    pass


class Query(
    carmanager.users.resolvers.queries.get_my_profile.Query,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query)
