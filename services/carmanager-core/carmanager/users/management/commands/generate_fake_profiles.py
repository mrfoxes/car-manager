import logging

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from faker import Faker

from carmanager.users.models import Profile, Preferences

User = get_user_model()


class Command(BaseCommand):
    help = "Generate fake profiles"
    fake = Faker(['it_IT'])

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def get_fake_profile(self):
        return self.fake.profile()

    def handle(self, *args, **options):
        count = options['count']

        for i in range(0, count):
            profile_data = self.get_fake_profile()

            email = profile_data.pop('mail')
            username = email
            text_password = 'password'
            name: str = profile_data.pop('name')
            first_name, last_name, *rest = name.split(' ')

            # Remove redundant fields
            profile_data.pop('current_location')
            profile_data.pop('website')
            profile_data.pop('username')

            user, created = User.objects.get_or_create(email=email)

            if created:
                try:
                    user.set_password(text_password)
                    user.username = username
                    user.first_name = first_name
                    user.last_name = last_name
                    user.save()

                    profile, created_profile = Profile.objects.get_or_create(user=user)
                    preferences, created_preferences = Preferences.objects.get_or_create(user=user)

                    Profile.objects.filter(id=profile.id).update(**profile_data)

                except Exception as e:
                    logging.error(e)
