import graphene
from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType
from graphql_jwt.decorators import login_required

User = get_user_model()


class UserObjectType(DjangoObjectType):
    full_name = graphene.String()

    class Meta:
        model = User
        name = 'User'
        exclude_fields = (
            'password',
            'car_property_owner',
            'car_property_new_owner',
        )

    def resolve_full_name(self: User, info):
        return f'{self.first_name} {self.last_name}'.strip()


class Query(object):
    get_my_profile = graphene.Field(UserObjectType)

    @login_required
    def resolve_get_my_profile(self, info, **kwargs):
        return info.context.user
