from .base import *

DEBUG = env.bool('DEBUG', default=True)

CORS_ORIGIN_ALLOW_ALL = True
