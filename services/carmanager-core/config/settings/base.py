"""
Django settings for carmanager project.

Generated by 'django-admin startproject' using Django 3.0.4.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

from datetime import timedelta
from carmanager.version import __version__, __name__
import environ

env = environ.Env()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('carmanager')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str('SECRET_KEY', default='bw_&nx5p1-rdwp74@z_g#%66wy00ogw%y!7q&-uu=_@$=y6h+2')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DEBUG', default=False)

CORS_ORIGIN_ALLOW_ALL = env.bool('CORS_ORIGIN_ALLOW_ALL', default=False)

ALLOWED_HOSTS = []

ENV_ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default='localhost,*,')

ALLOWED_HOSTS.extend(ENV_ALLOWED_HOSTS)
ALLOWED_HOSTS.append(env.str('DOMAIN', default=''))
ALLOWED_HOSTS.extend(env.list('DOMAIN_ALIASES', default=''))
ALLOWED_HOSTS.append(env.list('DOMAIN_REDIRECTS', default=''))

# Scout
SCOUT_MONITOR = env.str('SCOUT_MONITOR', default=False)
SCOUT_KEY = env.str('SCOUT_KEY', default=None)
SCOUT_NAME = __name__

# Sentry
ENABLE_SENTRY = env.bool('ENABLE_SENTRY', default=False)

SENTRY_DNS = env.str('SENTRY_DNS', default=None)

SENTRY_ENVIRONMENT = env.str('SENTRY_ENVIRONMENT', default='test')

if ENABLE_SENTRY and SENTRY_DNS:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration
    from sentry_sdk.integrations.celery import CeleryIntegration
    from sentry_sdk.integrations.redis import RedisIntegration

    sentry_sdk.init(
        release=f'{__name__}@{__version__}',
        dsn=SENTRY_DNS,
        integrations=[
            DjangoIntegration(),
            CeleryIntegration(),
            RedisIntegration(),
        ],
        environment=SENTRY_ENVIRONMENT
    )

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

INSTALLED_APPS += [
    'django_extensions',
    'graphene_django',
    'graphql_jwt',
    'corsheaders',
]

INSTALLED_APPS += [
    'carmanager.core',
    'carmanager.users',
    'carmanager.cars_database',
    'carmanager.cars_property'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

ASGI_APPLICATION = 'config.asgi.application'

# Email settings

EMAIL_BACKEND = env.str('EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')

EMAIL_HOST = env.str('EMAIL_HOST', default=None)
EMAIL_HOST_USER = env.str('EMAIL_HOST_USER', default=None)
EMAIL_HOST_PASSWORD = env.str('EMAIL_HOST_PASSWORD', default=None)
EMAIL_PORT = env.str('EMAIL_PORT', default=None)

DEFAULT_FROM_EMAIL = env.str('DEFAULT_FROM_EMAIL', default='noreply@carmanager.com')
SERVER_EMAIL = env.str('SERVER_EMAIL', default='noreply@carmanager.com')

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        **env.db('DATABASE_URL', 'postgresql://postgres:postgres@core_db/postgres'),
        'ATOMIC_REQUESTS': True,
        'CONN_MAX_AGE': 600
    },
}

# Cache
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': env.str('REDIS_CACHE_URL', default='redis://redis:6379/0'),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'CONNECTION_POOL_KWARGS': {'decode_responses': True},
        }
    }
}

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Authentication

AUTHENTICATION_BACKENDS = [
    'graphql_jwt.backends.JSONWebTokenBackend',
    'django.contrib.auth.backends.ModelBackend',
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('en', 'English'),
    ('it', 'Italian'),
)

LOCALE_PATHS = [
    str((ROOT_DIR.path('locale'))),
]

TIME_ZONE = env.str('TIMEZONE', default='Europe/Rome')

TIME_INPUT_FORMATS = [
    '%I:%M:%S %p',
    '%I:%M %p',
    '%I %p',
    '%H:%M:%S',
    '%H:%M:%S.%f',
    '%H:%M',
]

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = str(ROOT_DIR.path('staticfiles'))

MEDIA_URL = '/media/'

STATICFILES_DIRS = [
    str(ROOT_DIR.path('static')),
]

# Logging

LOGLEVEL = env.str('LOGLEVEL', 'DEBUG')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    "formatters": {
        "verbose": {
            "format": "[%(asctime)s] - %(levelname)s - %(name)s: %(message)s",
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
        "simple": {"format": "%(levelname)s %(message)s"},
    },
    'handlers': {
        'console': {
            'level': LOGLEVEL,
            'class': 'logging.StreamHandler',
            "formatter": "verbose",
        }
    },
    'loggers': {
        "django": {"handlers": ["console"], "propagate": False, "level": "INFO"},
        "django.server": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": "django.server",
        },
        'django.request': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'ERROR',
            'propagate': False
        },
        'daphne.ws_protocol': {
            'handlers': ['console'],
            'level': LOGLEVEL,
            'propagate': False
        },
        '': {
            'handlers': ['console'],
            'level': LOGLEVEL,
            'propagate': False
        },
    }
}

# Celery
CELERY_BROKER_URL = env.str('CELERY_BROKER_URL', default='redis://redis:6379/0')
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_RESULT_BACKEND = env.str('CELERY_RESULT_BACKEND', default='redis://redis:6379/0')
CELERY_TASK_SERIALIZER = 'json'
CELERY_ENABLE_UTC = False
CELERY_TIMEZONE = TIME_ZONE

# Graphql
GRAPHENE = {
    'SCHEMA': 'carmanager.core.schema.schema',
    'MIDDLEWARE': [
        'graphql_jwt.middleware.JSONWebTokenMiddleware',
    ],
}

GRAPHQL_JWT = {
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LONG_RUNNING_REFRESH_TOKEN': True,
    'JWT_EXPIRATION_DELTA': timedelta(**{
        str(k): int(v) for k, v in env.dict('JWT_EXPIRATION_DELTA', default={'seconds': 3600}).items()
    }),
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=2),
    'JWT_LEEWAY': timedelta(seconds=10),
    'JWT_AUTH_HEADER_PREFIX': 'Bearer'
}
