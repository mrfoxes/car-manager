import store from 'store'

export const TOKEN_AUTH = 'tokenAuth'

export function setAuthToken(token: string): void {
  store.set(TOKEN_AUTH, token)
}

export function getAuthToken(): string {
  return store.get(TOKEN_AUTH)
}

export function removeAuthToken(): void {
  store.remove(TOKEN_AUTH)
}

export function clearStorage(): void {
  store.clearAll()
}
