import React from 'react'
import ReactDOM from 'react-dom'
import './styles/index.css'
import './styles/carbon.scss'
import App from './App'
import log from 'loglevel'
import * as serviceWorker from './serviceWorker'
import Config from './libs/AppConfig'

async function loadConfig(): Promise<Object> {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await fetch('/static/config.json')
      const configData = await response.json()

      return resolve(configData)
    } catch (e) {
      return reject(new Error('Cannot load config. Check if config file exist.'))
    }
  })
}

async function main() {
  try {
    const configData = await loadConfig()

    Config.set(configData)

    const loglevel: string = Config.get('LOGLEVEL', 'ERROR')

    // @ts-ignore
    log.setDefaultLevel(log.levels[loglevel])

    log.info('Config loaded. Keys:', Object.keys(Config.get()))
  } catch (e) {
    log.error(e)
  }

  return ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById('root'),
  )
}

main()

serviceWorker.unregister()
