import React, { Component } from 'react'
import ErrorBoundary from './components/ErrorBoundary'
import { ApolloClient } from 'apollo-client'
import { HttpLink, InMemoryCache } from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'
import Box from 'ui-box'
import store from 'store'
import { ApolloLink } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import Config from './libs/AppConfig'
import log from 'loglevel'
import HealthCheck from './components/Health'
import { BrowserRouter } from 'react-router-dom'
import { renderRoutes } from 'react-router-config'
import { authenticatedRoutes, unAuthenticatedRoutes } from './components/Router/routes'
import { getAuthToken, removeAuthToken } from './config/authentication'
import { VERIFY_TOKEN } from './graphql/mutations'

interface IState {
  loading?: boolean
  tokenAuth?: any
  isRefreshingToken?: boolean
  isLoggedIn?: boolean
  client?: any
}

interface IProps {

}

class App extends Component<IProps, IState> {
  authLink = setContext((_, {headers}) => {
    const token = this.state.tokenAuth
    let canRefreshToken = false

    if (token) {
      //   Check token expiration
    }

    // App can refresh token
    if (canRefreshToken && !this.state.isRefreshingToken) {

    }

    const refreshToken = async () => {
      if (this.state.isRefreshingToken) {
        await new Promise(r => setTimeout(r, 150))
      }

      if (!this.state.isRefreshingToken) setTimeout(refreshToken, 0)
    }

    return {
      headers: {
        ...headers,
        'Authorization': token !== '' ? `Bearer ${token}` : '',
      },
    }
  })

  httpLink = new HttpLink({
    uri: Config.get('GRAPHQL_API_URL', null),
  })

  state = {
    tokenAuth: getAuthToken() || '',
    loading: true,
    isRefreshingToken: false,
    isLoggedIn: false,
    client: new ApolloClient({
      link: ApolloLink.from([
        this.authLink,
        this.httpLink,
      ]),
      cache: new InMemoryCache(),
    }),
  }

  verifyAuthToken = (token: string) => {
    return new Promise(async (resolve, reject) => {
      const {client} = this.state

      try {
        const response = await client.mutate({
          mutation: VERIFY_TOKEN,
          variables: {
            token,
          },
        })

        return resolve(token)
      } catch (e) {
        log.info(e)

        return reject(e)
      }
    })
  }

  refreshToken = async () => {
    return new Promise(resolve => resolve())
  }

  restoreSession = () => {
    return new Promise(async resolve => {
      log.debug('Start session restore')

      // Restore existing session
      try {
        const currentToken = getAuthToken()

        if (currentToken) {
          const authToken = await this.verifyAuthToken(currentToken)
          await this.refreshToken()

          if (authToken) {
            this.setState({
              isLoggedIn: true,
              tokenAuth: authToken,
            })
          } else {
            this.setState({
              isLoggedIn: false,
            })
          }
        }
      } catch (e) {
        // Start a new session

        removeAuthToken()
      }

      return resolve()
    })
  }

  componentDidMount = async () => {
    await this.restoreSession()

    setTimeout(() => {
      this.setState(this.setLoading(false))
    }, 350)
  }

  setLoading = (loading: boolean) => {
    return {
      loading,
    }
  }

  render() {
    const {
      loading,
      client,
      isLoggedIn,
    } = this.state

    if (loading) {
      return 'Loading...'
    }

    if (loading) {
      return <Box>Loading...</Box>
    }

    return (
      <ErrorBoundary>
        {!loading && (
          <ApolloProvider client={client}>
            <HealthCheck>
              <BrowserRouter>
                {isLoggedIn && renderRoutes(authenticatedRoutes)}
                {!isLoggedIn && renderRoutes(unAuthenticatedRoutes)}
              </BrowserRouter>
            </HealthCheck>
          </ApolloProvider>
        )}
      </ErrorBoundary>
    )
  }
}

export default App
