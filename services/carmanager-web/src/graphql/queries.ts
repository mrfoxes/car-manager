import { gql } from 'apollo-boost'

export const HEALTH = gql`
    query Healt {
        health
    }
`

export const GET_MY_CARS = gql`
    query GetMyCars {
        getMyCars {
            id
            licensePlate
            owner {
                id
                email
            }
            car {
                id
                make
                model
            }
            status
        }
    }
`

export const GET_MY_NAME = gql`
    query GetMyProfile {
        getMyProfile {
            id
            firstName
            lastName
            fullName
        }
    }
`
