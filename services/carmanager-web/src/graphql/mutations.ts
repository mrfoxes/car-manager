import { gql } from 'apollo-boost'

export const TOKEN_AUTH = gql`
    mutation TokenAuth($username: String!, $password: String!) {
        tokenAuth(username: $username, password: $password) {
            token
        }
    }
`

export const VERIFY_TOKEN = gql`
    mutation VerifyToken($token: String!) {
        verifyToken(token:$token) {
            payload
        }
    }
`

export const TRANSFER_CAR_PROPERTY = gql`
    mutation Transfer($input: TransferCarPropertyInput!) {
        transferCarProperty(input: $input) {
            success
            errors {
                field
                messages
            }
        }
    }
`
