class AppConfig {
  private _config: any

  constructor() {
    this._config = {}
  }

  set(config: object) {
    return this._config = {
      ...this._config,
      ...config,
    }
  }

  get(key: string = '', defaultValue: any = undefined): any {
    if (key === '') {
      return this._config
    } else {
      return this._config[key] || defaultValue
    }
  }
}

const Config = new AppConfig()

export default Config
