import React from 'react'
import Box from 'ui-box'

interface IProps {
  lineTitle: string
  lineData: string
}

const CarInfoLine = (props: IProps) => {
  const {
    lineTitle,
    lineData,
  } = props

  return (
    <div>
      <Box>
        <h6><strong>{lineTitle}:</strong></h6>
        <span>{lineData}</span>
      </Box>
    </div>
  )
}

export default CarInfoLine
