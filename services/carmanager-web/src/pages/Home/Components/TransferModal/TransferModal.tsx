import React, { ChangeEvent, useRef, useState } from 'react'
import { DataShare16 } from '@carbon/icons-react'
import Box from 'ui-box'
import { Checkbox, Form, FormGroup, InlineNotification, ModalWrapper, TextInput } from 'carbon-components-react'
import { IVehicleInfo } from '../../types'
import CarInfoLine from './CarInfoLine'
import { useMutation } from '@apollo/react-hooks'
import { TRANSFER_CAR_PROPERTY } from '../../../../graphql/mutations'
import log from 'loglevel'
import { GET_MY_CARS } from '../../../../graphql/queries'

interface IProps {
  vehicleInfo: IVehicleInfo
}

interface ITransferFormData {
  licensePlate?: string
  newOwner?: string
  accept?: boolean
}

const TransferModal = (props: IProps) => {
  const {
    vehicleInfo,
  } = props

  const [transferCarProperty, {loading}] = useMutation(TRANSFER_CAR_PROPERTY)
  const [errors, setErrors] = useState<Array<any>>([])
  const [success, setSuccess] = useState<boolean>(false)
  const [error, setError] = useState<string | false>(false)
  const [transferFormData, setTransferFormData] = useState<ITransferFormData>({
    accept: false,
    newOwner: '',
    licensePlate: '',
  })

  const handleTransferRequest = async () => {
    setError(false)

    try {
      const response = await transferCarProperty({
        variables: {
          input: transferFormData,
        },
        refetchQueries: [{
          query: GET_MY_CARS,
        }],
      })

      log.debug(response)

      const errorsData: any = response?.data?.transferCarProperty?.errors
      const successData: any = response?.data?.transferCarProperty?.success

      setSuccess(successData)

      if (errorsData) {
        setErrors(errorsData)
      }
    } catch (e) {
      const errorData = e.graphQLErrors?.[0]?.message || e.message

      setError(errorData)

      log.error(e)
    }
  }

  const handleTransfer = (): boolean => {
    handleTransferRequest()
    return true
  }

  const handleTransferFormChange = (event: ChangeEvent<HTMLInputElement> | any) => {
    const {target: {id, value}} = event

    const labelId = id.split(':')[0]

    setTransferFormData({
      ...transferFormData,
      [labelId]: value,
    })
  }

  const isNewOwnerValid = () => {
    const newOwner = transferFormData.newOwner.trim()

    return newOwner !== ''
  }

  const isTransferFormValid = () => {
    return transferFormData.accept && isNewOwnerValid()
  }

  const isLicensePlateValid = () => {
    const licensePlate = transferFormData.licensePlate.trim()

    return licensePlate === vehicleInfo.licensePlate
  }

  const canSubmit = () => {
    return isTransferFormValid() && isLicensePlateValid() && !loading
  }

  const getFieldErrorMessage = (field: string): string => {
    return errors.filter((error: any) => {
      return error?.field === field
    })[0]?.messages || ''
  }

  const hasValidError = (field: string) => {
    return getFieldErrorMessage(field) !== ''
  }

  return (
    <>
      <Form aria-label="Transfer property">
        {/*
       // @ts-ignore */}
        <ModalWrapper
          buttonTriggerClassName={`${vehicleInfo.status !== 'TRANSFERRED' ? 'bx--btn--disabled' : ''}`}
          iconDescription="Transfer"
          triggerButtonIconDescription="Transfer"
          triggerButtonKind="danger"
          buttonTriggerText="Transfer"
          renderTriggerButtonIcon={DataShare16}
          primaryButtonDisabled={!canSubmit() || success}
          primaryButtonText="Transfer"
          modalLabel="Transfer vehicle property"
          modalHeading="ATTENTION"
          danger
          handleSubmit={handleTransfer}
        >
          <Box>
            <p>You're about to evade a request to transfer the car property to another user.</p>
            <br />
            <Box marginBottom={3}>
              <h4 className=""><strong>Car Info</strong></h4>
            </Box>
            <Box marginBottom={3}>
              <CarInfoLine lineTitle="License plate" lineData={vehicleInfo.licensePlate} />
            </Box>
            <Box marginBottom={3}>
              <CarInfoLine lineTitle="Car Make" lineData={vehicleInfo.car.make} />
            </Box>
            <Box marginBottom={3}>
              <CarInfoLine lineTitle="Car Model" lineData={vehicleInfo.car.model} />
            </Box>
            <Box border="1px solid #696969" marginY={10} />
            <Box marginBottom={3}>
              <h4 className=""><strong>Transfer</strong></h4>
              {success && (
                <InlineNotification
                  hideCloseButton
                  lowContrast
                  kind="success"
                  subtitle={<span>Request successfully sent</span>}
                  title="Transfer"
                />
              )}
              {error && (
                <InlineNotification
                  hideCloseButton
                  lowContrast
                  kind="error"
                  subtitle={<span>{error}</span>}
                  title="Error"
                />
              )}
            </Box>
            <FormGroup legendText="">
              <TextInput
                id={`currentOwner:${vehicleInfo.id}`}
                labelText="Current owner"
                value={vehicleInfo.owner.email}
                disabled
              />
              <Box display="flex" justifyContent="center" marginY={10}>
                <DataShare16 />
              </Box>
              <TextInput
                helperText="Insert the email address for the new user"
                id={`newOwner:${vehicleInfo.id}`}
                labelText="New owner"
                placeholder="Username / Email"
                onChange={handleTransferFormChange}
                invalid={!isNewOwnerValid() || hasValidError('new_owner')}
                invalidText={getFieldErrorMessage('new_owner')}
                disabled={success}
                required
              />
              <br />
              <TextInput
                helperText="Insert the license plate"
                id={`licensePlate:${vehicleInfo.id}`}
                disabled={success}
                labelText="License plate"
                placeholder="License plate"
                onChange={handleTransferFormChange}
                invalid={!isLicensePlateValid() || hasValidError('license_plate')}
                required
              />
            </FormGroup>
            <fieldset className="bx--fieldset">
              <legend className="bx--label">Accept to proceed</legend>
              <Checkbox
                disabled={success}
                required
                labelText={`I agree to start the property transfer for this vehicle: ${vehicleInfo.licensePlate}`}
                id={`accept:${vehicleInfo.id}`}
                value={0}
                onChange={(value) => handleTransferFormChange({target: {id: 'accept', value}})} />
            </fieldset>

          </Box>
        </ModalWrapper>
      </Form>
    </>
  )
}

export default TransferModal
