import React from 'react'
import { gql } from 'apollo-boost'
import { Query } from '@apollo/react-components'
import Box from 'ui-box'
import { GET_MY_NAME } from '../../../graphql/queries'

export interface IGetMyName {
  id: string
  firstName: string
  lastName: string
  fullName: string
}

export interface IResult {
  data?: any
  loading: boolean
}

const WelcomeInfo = () => {
  return (
    <Query query={GET_MY_NAME} fetchPolicy="cache-first">
      {({data, loading}: IResult) => {
        if (loading) {
          return null
        }

        const profileData: IGetMyName = data?.getMyProfile

        if (profileData) {
          return (
            <Box>
              <h2>Benvenuto{`${profileData.fullName ? ` ${profileData.fullName}` : ''}`}</h2>
            </Box>
          )
        }
      }}
    </Query>
  )
}

export default WelcomeInfo
