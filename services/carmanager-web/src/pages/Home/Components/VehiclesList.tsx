import React from 'react'
import {
  DataTable,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableExpandedRow,
  TableExpandHeader,
  TableExpandRow,
  TableHead,
  TableHeader,
  Tooltip,
  TableRow, ModalWrapper, Select,
} from 'carbon-components-react'
import { DataShare16 } from '@carbon/icons-react'
import { map } from 'ramda'
import * as R from 'ramda'
import { useQuery } from '@apollo/react-hooks'
import { GET_MY_CARS } from '../../../graphql/queries'
import Box from 'ui-box'
import TransferModal from './TransferModal/TransferModal'

const headerData = [
  {
    header: 'Targa',
    key: 'licensePlate',
  },
  {
    header: 'Make',
    key: 'car_make',
  },
  {
    header: 'Model',
    key: 'car_model',
  },
  {
    header: 'Status',
    key: 'status',
  },
  {
    header: 'Actions',
    key: 'actions',
  },
]

const VehiclesList = () => {
  const {data, loading} = useQuery(GET_MY_CARS)

  const rowData = data?.getMyCars || []

  const conformedRowsData = map(rowInfo => {
    const data = {
      ...rowInfo,
    }

    const carData = {
      ...data.car,
    }

    Object.keys(carData).forEach((key: string) => {
      data[`car_${key}`] = carData[key]
    })

    return data
  }, rowData)

  const rowsById = R.indexBy(R.prop('id'), conformedRowsData)

  return (
    <DataTable
      rows={conformedRowsData}
      headers={headerData}
      render={({rows, headers, getHeaderProps, getRowProps, getTableProps}) => (
        <TableContainer title="My vehicles">
          <Table {...getTableProps()}>
            <TableHead>
              <TableRow>
                {headers.map(header => (
                  <TableHeader {...getHeaderProps({header})}>
                    {header.header}
                  </TableHeader>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => {
                const currentItem = rowsById[row.id]

                return (
                  <React.Fragment key={row.id}>
                    {/*
                      // @ts-ignore */}
                    <TableRow {...getRowProps({row})}>
                      {row.cells.map((cell: any) => {
                        if (cell.info.header === 'actions') {
                          return (
                            <TableCell key={cell.id}>
                              <TransferModal vehicleInfo={currentItem} />
                            </TableCell>
                          )
                        }

                        return (
                          <TableCell key={cell.id}>
                            <Box display="flex" height="100%" width="100%" alignItems="center">
                              {cell.value}
                            </Box>
                          </TableCell>
                        )
                      })}
                    </TableRow>
                    {/*
                      // @ts-ignore */}
                    {row.isExpanded && (
                      <TableExpandedRow colSpan={headers.length + 1}>
                      </TableExpandedRow>
                    )}
                  </React.Fragment>
                )
              })}
            </TableBody>
          </Table>
          {rows.length === 0 && (
            <Box display="flex" width="100%" padding={20} backgroundColor="#fff" justifyContent="center">
              No registered vehicles
            </Box>
          )}
        </TableContainer>
      )} />
  )
}

export default VehiclesList
