import React from 'react'
import WelcomeInfo from './Components/WelcomeInfo'
import Box from 'ui-box'
import VehiclesList from './Components/VehiclesList'

const Home = () => {
  return (
    <Box padding={20}>
      <Box>
        <WelcomeInfo />
      </Box>
      <Box marginTop={20}>
        <VehiclesList />
      </Box>
    </Box>
  )
}

export default Home
