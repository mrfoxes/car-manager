export interface ICar {
  id: string
  make: string
  model: string
}

export interface IOwner {
  id: string
  email: string
}

export interface IVehicleInfo {
  id: string
  licensePlate: string
  status: string
  owner: IOwner
  car: ICar
}
