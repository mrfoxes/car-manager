import React from 'react'
import Box from 'ui-box'
import { Breadcrumb, BreadcrumbItem } from 'carbon-components-react'
import { Link } from 'react-router-dom'

const Profile = () => {
  return (
    <Box padding={20}>
      <Box marginBottom={10}>
        <Breadcrumb>
          <BreadcrumbItem href="#">
            <Link to="/">
              Home
            </Link>
          </BreadcrumbItem>
          <BreadcrumbItem href="#">
            <Link to="/profile">
              Profile
            </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </Box>
      <Box>
        Profile
      </Box>
    </Box>
  )
}

export default Profile
