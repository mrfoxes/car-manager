import React, { ChangeEvent, FormEvent, useState } from 'react'
import { Button, Form, InlineLoading, Link, Loading, TextInput } from 'carbon-components-react'
import Box from 'ui-box'
import './LoginForm.css'
import { useMutation } from '@apollo/react-hooks'
import { TOKEN_AUTH } from '../../../graphql/mutations'
import { GraphQLError } from 'graphql'
import { setAuthToken } from '../../../config/authentication'

const SPACING = 50

interface ITokenAuth {
  token?: string
}

interface ITokenAuthData {
  tokenAuth?: ITokenAuth
}

interface IError {
  error: string
}

interface IProps {
  onLogin: Function | undefined
}

const LoginForm = (props: IProps) => {
  const {
    onLogin,
  } = props

  const [callTokenAuth] = useMutation<any>(TOKEN_AUTH)
  const [isLoggingIn, setIsLoggingIn] = useState(false)
  const [error, setError] = useState<IError | null>(null)
  const [errorMessage, setErrorMessage] = useState<IError | null>(null)
  const [loginData, setLoginData] = useState({username: null, password: null})

  const handleLoginFormSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    performLogin()
  }

  const handleOnInputLoginForm = (event: ChangeEvent<HTMLInputElement>) => {
    const {target: {value, id}} = event

    if (errorMessage) {
      setErrorMessage(null)
      setError(null)
    }

    setLoginData({
      ...loginData,
      [id]: value,
    })
  }

  const performLogin = async () => {
    setIsLoggingIn(true)
    try {
      const {data: {tokenAuth}}: { data?: ITokenAuthData } = await callTokenAuth({
        variables: {
          ...loginData,
        },
      })

      setAuthToken(tokenAuth.token)
      if (onLogin) {
        onLogin()
      }
    } catch (e) {
      const errorData = e.graphQLErrors?.[0]?.message || e.message

      setError(errorData)
      setErrorMessage(errorData)

      setTimeout(() => {
        setIsLoggingIn(false)
      }, 200)
    }
  }

  const isError = () => {
    return !!error
  }

  return (
    <Box boxShadow="0px 1px 7px #69696960" borderRadius={5} padding={SPACING}>
      <Box marginBottom={SPACING / 2}>
        <h2>
          Login
        </h2>
      </Box>
      <Box>
        <Form onSubmit={handleLoginFormSubmit}>
          <Box marginBottom={SPACING / 4}>
            <TextInput
              invalid={isError()}
              disabled={isLoggingIn}
              required
              id="username"
              onChange={handleOnInputLoginForm}
              labelText=""
              placeholder="Username / Email"
            />
          </Box>
          <TextInput
            required
            invalid={isError()}
            disabled={isLoggingIn}
            id="password"
            onChange={handleOnInputLoginForm}
            invalidText=""
            labelText=""
            placeholder="Password"
            type="password"
          />
          {errorMessage && (
            <Box marginTop={SPACING / 2} display="flex" justifyContent="center">
            <span style={{color: '#da1e28'}}>
              {error}
            </span>
            </Box>
          )}
          <Box display="flex" marginTop={SPACING / 2} height={50} justifyContent="center" alignContent="center">
            {!isLoggingIn && (
              <Button
                style={{
                  maxWidth: '100%',
                  width: '100%',
                  padding: 0,
                  justifyContent: 'center',
                }}
                kind="primary"
                tabIndex={0}
                type="submit"
                disabled={isLoggingIn}
              >
                <h5>Login</h5>
              </Button>
            )}
            {isLoggingIn && <Box display="flex" justifyContent="center" alignItems="center">
              {<InlineLoading style={{color: '#fff !important'}} description="Logging in..." />}
            </Box>}
          </Box>
        </Form>
        <Box marginTop={SPACING / 3} display="flex" flexDirection="row" justifyContent="center">
          <Link href="#">
            Aiuto
          </Link>
          <Box marginX={5}>
            &#183;
          </Box>
          <Link href="#">
            Registrati
          </Link>
        </Box>
      </Box>
    </Box>
  )
}

export default LoginForm
