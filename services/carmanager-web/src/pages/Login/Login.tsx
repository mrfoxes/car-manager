import React from 'react'
import Box from 'ui-box'
import LoginForm from './Components/LoginForm'
import PageCenter from '../../components/Layout/PageCenter'
import { RouteComponentProps } from 'react-router'
import * as H from 'history'

const LOGIN_FORM_WIDTH = 400

interface IProps {
}

const Login = (props: RouteComponentProps<IProps>) => {
  const {} = props

  const handleOnLogin = () => {
    window.location.reload()
  }

  return (
    <PageCenter>
      <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
        <Box display="flex" justifyContent="center" marginBottom={50}>
          <h1>
            Car Manager
          </h1>
        </Box>
        <Box maxWidth={LOGIN_FORM_WIDTH} width={LOGIN_FORM_WIDTH} marginBottom={100}>
          <LoginForm onLogin={() => handleOnLogin()} />
        </Box>
      </Box>
    </PageCenter>
  )
}

export default Login
