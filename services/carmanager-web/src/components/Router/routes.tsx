import { RouteConfig } from 'react-router-config'
import { Redirect } from 'react-router-dom'
import Login from '../../pages/Login/Login'
import React from 'react'
import Home from '../../pages/Home/Home'
import Logout from '../../pages/Logout/Logout'
import RootLayout from '../Layout/RootLayout'
import Profile from '../../pages/Profile/Profile'

export const unAuthenticatedRoutes: Array<RouteConfig> = [
  {
    component: Login,
    path: '/login',
  },
  {
    component: () => <Redirect to="/login" />,
    path: '/',
  },
]

export const authenticatedRoutes: Array<RouteConfig> = [
  {
    component: Logout,
    path: '/logout',
  },
  {
    component: () => <Redirect to="/" />,
    path: '/login',
  },
  {
    component: RootLayout,
    path: '/',
    routes: [
      {
        component: Home,
        path: '/',
        exact: true,
      },
      {
        component: Profile,
        path: '/profile',
        exact: true,
      },
    ],
  },
]
