import React, { Component, ErrorInfo, Fragment } from 'react'

interface IProps {

}

interface IState {
  error: Error | undefined
  errorInfo?: ErrorInfo | undefined
}

class ErrorBoundary extends Component<IProps, IState> {
  static defaultProps: IProps = {}

  state: Readonly<IState> = {
    error: undefined,
    errorInfo: undefined,
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo,
    })
  }

  render() {
    const {error, errorInfo} = this.state
    const {children} = this.props

    if (error) {
      return (
        <Fragment>
          <h2>Something went wrong.</h2>
          <p>{error && error.toString()}</p>
          <p>{errorInfo && errorInfo.componentStack}</p>
        </Fragment>
      )
    }

    return children
  }
}

export default ErrorBoundary
