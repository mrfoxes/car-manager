import React, { useEffect, useState } from 'react'
import Box from 'ui-box'
import './RootLayout.css'
import { renderRoutes } from 'react-router-config'
import { RouteComponentProps } from 'react-router'

import HeaderContainer from 'carbon-components-react/lib/components/UIShell/HeaderContainer'
import { Header } from 'carbon-components-react/lib/components/UIShell'

import { OverflowMenuItem, Search } from 'carbon-components-react'
import { removeAuthToken } from '../../config/authentication'
import { cleanCurrentSession } from '../../config/session'
import { Notification32 } from '@carbon/icons-react'
import { GET_MY_NAME } from '../../graphql/queries'
import { Query } from '@apollo/react-components'
import { IGetMyName, IResult } from '../../pages/Home/Components/WelcomeInfo'
import { Link } from 'react-router-dom'

interface IProps extends RouteComponentProps {
  route?: any
}

const RootLayout = (props: IProps) => {
  const {
    route,
    history,
  } = props

  const [showProfileDropdown, setShowProfileDropdown] = useState<boolean>(false)

  const logout = () => {
    removeAuthToken()
    cleanCurrentSession()

    window.location.reload()
  }

  const toggleProfileDropdown = () => {
    setShowProfileDropdown(!showProfileDropdown)
  }

  const closeProfileDropdown = () => {
    setShowProfileDropdown(false)
  }

  const handleProfileDropDownAutoClose = (e: any) => {
    const targetId = e?.target?.id
    if (targetId !== 'profile' && targetId !== 'profile_name') {
      closeProfileDropdown()
    }
  }

  useEffect(() => {
    document.addEventListener('click', handleProfileDropDownAutoClose)

    return () => document.removeEventListener('click', handleProfileDropDownAutoClose)
  })

  return (
    <Box>
      <Box marginBottom={50}>
        <HeaderContainer render={() => {
          return (
            <>
              <Header aria-label="Car Manager" style={{
                backgroundColor: '#fff',
                borderColor: '#fff',
                boxShadow: '0 1px 10px #69696940',
              }}
              >
                <Box display="flex" justifyContent="space-between" width="100%" flexDirection="row" paddingX={10}>
                  <Box display="flex" justifyContent="center" alignItems="center">
                  </Box>
                  <Box display="flex" width={400}>
                    <Search
                      small
                      labelText="Search"
                      id="search-main"
                      placeHolderText="Search"
                    />
                  </Box>
                  <Box display="flex">
                    <Box display="flex" height={50} justifyContent="center" alignItems="center">
                      <Box
                        id="profile"
                        is="button"
                        display="flex"
                        backgroundColor="orange"
                        width={35}
                        height={35}
                        borderRadius={50}
                        onClick={toggleProfileDropdown}
                        justifyContent="center"
                        alignItems="center"
                        border={0}
                        pointerEvents="all"
                      >
                        <Query query={GET_MY_NAME} fetchPolicy="cache-first">
                          {({data, loading}: IResult) => {
                            if (loading) {
                              return null
                            }

                            const profileData: IGetMyName = data?.getMyProfile

                            if (profileData) {
                              return (
                                <Box id="profile_name" pointerEvents="all" onClick={toggleProfileDropdown}>
                                  {`${profileData.firstName[0] + profileData.lastName[0]}`}
                                </Box>
                              )
                            }
                          }}
                        </Query>
                      </Box>
                      {showProfileDropdown && (
                        <Box position="absolute" width={200} right={0}>
                          <ul className="bx--overflow-menu-options bx--overflow-menu-options--open"
                              style={{right: 8, left: 'auto'}}>
                            <OverflowMenuItem
                              itemText="Profile"
                              onClick={() => history.push('/profile')}
                            />
                            <OverflowMenuItem
                              itemText="Logout"
                              requireTitle
                              isDelete
                              onClick={logout}
                            />
                          </ul>
                        </Box>
                      )}
                    </Box>
                  </Box>
                </Box>
              </Header>
            </>
          )
        }} />
      </Box>
      {renderRoutes(route.routes)}
    </Box>
  )
}

export default RootLayout
