import React from 'react'
import Box from 'ui-box'

interface IProps {
  children: any
}

const PageCenter = (props: IProps) => {
  const {
    children,
  } = props
  return (
    <Box display="flex" width="100%" height="100%" position="absolute" justifyContent="center" alignItems="center">
      {children}
    </Box>
  )
}

export default PageCenter
