import { useQuery } from '@apollo/react-hooks'
import { HEALTH } from '../graphql/queries'
import log from 'loglevel'

interface IProps {
  children: any
}

const HealthCheck = (props: IProps): any => {
  const {
    children,
  }: IProps = props

  const {data, loading, error, startPolling, stopPolling} = useQuery(HEALTH, {
    fetchPolicy: 'network-only',
  })

  if (loading) {
    return null
  }

  if (data?.health) {
    log.info('Health check', data)

    return children
  }

  throw Error('Service not healthy')
}

export default HealthCheck
