#!/usr/bin/env sh

if [ -z $APP_HOME ]; then
    APP_HOME="\/usr\/share\/nginx\/html"
fi

if [ -z $GRAPHQL_API_URL ]; then
    GRAPHQL_API_URL=""
fi

if [ -z $LOGLEVEL ]; then
    LOGLEVEL="DEBUG"
fi

CONFIG_PATH=${APP_HOME}/static

sed -i "s/{{ GRAPHQL_API_URL }}/${GRAPHQL_API_URL}/" ${CONFIG_PATH}/config.json
sed -i "s/{{ LOGLEVEL }}/${LOGLEVEL}/" ${CONFIG_PATH}/config.json

nginx -g "daemon off;"
