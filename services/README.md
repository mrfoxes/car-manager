# Development #

## Services

    ├── carmanager-core
    └── carmanager-web

## Docker Compose cheat-sheet

Start services

    docker-compose up -d
    
Start a service with a command

    docker-compose run {{SERVICE_NAME}} {{COMMAND}}
    
Override container entrypoint

    docker-compose run --entrypoint /usr/bin/fish core
    
Exec a command in a service

    docker-compose exec {{SERVICE_NAME}} {{COMMAND}}
    
Build service

    docker-compose build {{SERVICE_NAME}}

Stop

    docker-compose stop

Clean 

    docker-compose down --volume --remove-orphans
